/*RAÚL_HERNÁNDEZ_LÓPEZ
 * freeenrgy1975@gmail.com
 *17 de octubre del 2020*/

/*Calcular el nuevo salario de un empleado si se le descuenta el 20% de
 *  su salario actual.*/

#include <iostream>
using namespace std;
/*Inicio metodo main*/
int main(){
   /*Declaracion de variables*/
   float Salario_Actual, Descuento, Salario_Descuento;
   cout << "Salario actual :";
   /*Calcula el sueldo del empleado con el descuento del 20%*/
   cin >> Salario_Actual;
   Salario_Descuento = Salario_Actual * 0.80;
   Descuento = Salario_Actual * 0.20;
   /*Impresion de resultados*/
   cout << "\nEl descuento es igual a $" << Descuento 
   << "\nMonto a cobrar $" << Salario_Descuento;
   
}

