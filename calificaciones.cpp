/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Miercoles 19 de agosto del 2020*/

/*Asigna una letra segun la calificacion obtenida, si el alumno tiene de 9 a 10 de calificacion se
 *le asignara un A, si tiene de 8 a 8.9 se le asigna una b, si tiene de 7 a 7.9 una  c, en caso de
 *de  que  su  calificacion  ronde  de  6  a  6.9  una  D  y  si tiene menos se le asigna una F */

#include <iostream>
#include <string>
using namespace std;

int main(){
    //define como valor predeterminado seleccion como false 
    bool seleccion = false;

    do{
	//Declaracion de variables y recopilacion de datos
	float calificacion;
	cout << "\nIngrese su calificacion en un rango del 1 al 10 :";
	cin >> calificacion;
	string mensaje = "\nHas obtenido ";

	//Impresion de resultados con base a una serie de condicionales
	if ((calificacion >= 0) && (calificacion <= 10)){
            seleccion = false;

   	    if ((calificacion <= 10)&&(calificacion >= 9)) {
		cout << mensaje<< "es A eres mi idolo!\n";
	    }//Fin if
	    else if ((calificacion >= 8) && (calificacion < 9)) {
		cout << mensaje << "B sigue asi!\n";
	    }//Fin else if 1

	    else if ((calificacion >= 7) && (calificacion < 8)) {
		cout << mensaje << "C puedes mejorar vamos!\n";
	    }//Fin else if 2
	    else if ((calificacion >= 6) && (calificacion < 7)) {
		cout << mensaje<< "D tienes muchas areas de oportunidades\n";
	    }//Fin else if 3
	    else if (calificacion <= 5) { 
		cout << mensaje << "F eres un fracaso!\n";
	    }//Fin else if 4
	    else{
	        cout << "\nlos valores ingresados no son validos "
                << "por favor \ningrese un dato valido\n";
	        seleccion = true;
	    }//fin else
	}
	/*En caso de que el valor ingresado este fuera del rago imprime un mensaje y 
	ejecuta el programa de nuevo*/ 
	else{
		cout << "\nlos valores ingresados no estan dentro del rango\n"
		     << "establecido, reiniciando, espere un momento por favor...\n"
		     << "________________________________________________________\n";
		seleccion = true;
	}//fin else   
    }while(seleccion == true);//Fin ciclo do while
}//fin del metodo main	
