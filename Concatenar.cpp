/*RAÚL_HERNÁNDEZ_LÓPEZ
 * freeenregy1975@gmail.com
 * 20 de octubre del 2020*/

/*Leer tres números enteros de un Digito y almacenarlos en una
sola variable que contenga a esos tres dígitos Por ejemplo
si A=5 y B=6 y C=2 entonces X=562*/

#include <iostream>
#include <string>
using namespace std;
/*Inicio metodo main*/
int main(){
    /**/ 
    string cadena_1, cadena_2, cadena_3, cadena_4;
    cout << "Ingresa un numero o letra :";
    cin >> cadena_1;
    cout << "Ingresa otro numero o letra :"; 
    cin >> cadena_2;
    cout << "Ingresa otro numero o letra :";
    cin >> cadena_3;
    cadena_4 = cadena_1 + cadena_2 + cadena_3;
    cout << "Valores concatenados [" << cadena_4 << "]\n";
}
