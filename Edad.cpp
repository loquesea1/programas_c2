/*RAÚL_HERNÁNDEZ_LÓPEZ
* freeenrgy1975@gmail.com
* 20 de octubre del 2020*/

/*Obtener la edad de una persona en meses, si se ingresa su edad en años y meses.
 *Ejemplo: Ingresado 3 años 4 meses debemostrar 40 meses.*/

#include <iostream>
using namespace std;

int main(){
  /*Declaracion de variables*/
  int Meses, Edad_Meses, Años;
  /*Recopila datos*/
  cout << "\nIngresa tu edad en Años :";
  cin >> Años;
  cout << "\nIngresa el numero de meses restantes :";
  cin >> Meses;
  /*calcula el numero de meses en los años y los suma con los meses restantes*/
  Edad_Meses = (Años * 12) + Meses;
  /*Imprime resultados*/
  cout << "\nEdad en Meses [" << Edad_Meses << "]\n";
}/*fin del metodo main*/                         
