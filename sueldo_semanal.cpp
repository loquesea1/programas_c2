/*Raul_Hernandez_Lopez*/
/*freeenergy1975@gmail.com*/
/*10 de octubre del 2020*/
#include <iostream>
using namespace std;

int main(){
	/*Declaracion de variables*/
	float Horas_Trabajadas, Sueldo_Hora, Sueldo_Semanal, Sueldo_Final, Horas_Pago;
	int x, Dias_Trabajados, Numero_Trabajadores;
	cout << "\nNúmero de trabajadores :";
	cin >> Numero_Trabajadores;
	/*Almacena los nombres de los trabajadores*/
	char Nombre_Trabajadores[Numero_Trabajadores][30];
	
	for (x = 0; x <= Numero_Trabajadores - 1; x++){
		/*Recopilacion de datos*/
		cout << "\nNombre :";
		cin >> Nombre_Trabajadores[x];
		cout << "\nPago por hora $";
		cin >> Sueldo_Hora;
		cout << "\nHoras trabajadas :";
		cin >> Horas_Trabajadas;
		cout << "\nDias trabajados :";
		cin >> Dias_Trabajados;
		/*calcula el valor del sueldo por semana*/
		Sueldo_Semanal = (Sueldo_Hora * Horas_Trabajadas) * Dias_Trabajados;

		if ((Sueldo_Semanal >= 0 ) & (Sueldo_Semanal <= 150)){
			Sueldo_Final = Sueldo_Semanal * 0.95;
			cout << "\nSueldo total [$" << Sueldo_Final << "]\n";
		}/*Fin if*/
		else if ((Sueldo_Semanal > 150) & (Sueldo_Semanal < 300)){
			Sueldo_Final = Sueldo_Semanal * 0.97;
			cout << "\nSueldo total [$" << Sueldo_Final << "]\n";
		}/*Fin else if 1*/
		else if ((Sueldo_Semanal >= 300) & (Sueldo_Semanal < 450)){
			Sueldo_Final = Sueldo_Semanal * 0.91;
			cout << "\nSueldo total [$" << Sueldo_Final << "]\n";
		}/*Fin else if 2*/
	}/*Fin ciclo For*/
}/*Fin del metodo main*/
