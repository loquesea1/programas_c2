/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema: Genera el factorial de un número*/

#include<iostream>

using namespace std;

int main(){
    //Declaracion de variables.
    int x, numero, factorial = 1;

    cout << "\nCalculando el factorial de un número\n";

    //Almacena el valor del numero al cual se aplica el factorial.
    cout << "\nIngresa un número entero positivo: ";
    cin >> numero;


    for(x = numero; x >= 1; x--){
        //Calcula el factorial.    
        factorial *= x;
        //Impresion de resultados.
        cout << "El factorial de " << x << " = " << factorial << "\n";
    }//Fin for
}//Fin metodo main

