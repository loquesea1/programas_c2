/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Viernes 16 de octubre del 2020*/

/*En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. El presupuesto anual del
manera: Pediatría 42% y Traumatología 21%.*/

#include <iostream>
using  namespace std;
/*Inicio metodo main*/
int main(){
   /*Declaracion de variables*/
   float Presupuesto, Presupuesto_Pediatria, Presupuesto_Traumatologia, Presupuesto_Urgencias;
   /*Recopila datos*/
   cout << "Ingresa el presupuesto total:";
   cin >> Presupuesto;
   Presupuesto_Pediatria = Presupuesto * 0.42;
   Presupuesto_Traumatologia = Presupuesto * 0.12;
   Presupuesto_Urgencias = Presupuesto - (Presupuesto_Pediatria + Presupuesto_Traumatologia);
   /*Impresion de resultados*/
   cout << "\nLa distribucion del presupuesto queda de la siguiente manera :";
   cout << "\nPresupuesto pedriatia [$" << Presupuesto_Pediatria << "]";
   cout << "\nPresupuesto traumatologia [$" << Presupuesto_Traumatologia << "]";
   cout << "\nPresupuesto Urgencias [$" << Presupuesto_Urgencias << "]";
}

