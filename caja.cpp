/*Raul_Hernandez_Lopez*/
/*freeenergy1975@gmail.com*/
/*18 agosto del 2020*/

/*Calcula el total de una caja registradora*/
#include <iostream>
using namespace std;
int main(){
	/*Declaracion de variables*/
	int variedadB, variedadM, suma = 0, suma_M = 0, x, y, total;
	
	cout << "\ncuantos billetes de distinto valor tienes? : ";
	cin>> variedadB;

	for(x = 1; x <= variedadB; x++){
		/*Obtiene datos*/
		int valor, cantidad;
		cout << "\nCual es el valor del billete ? :$";
		cin >> valor;
		cout << "\ncuantos billetes con ese valor tienes? :";
		cin >> cantidad;
		suma = suma + ( valor * cantidad);
		}/*Fin ciclo for 1*/

		cout << "\ncuantas monedas de distinto valor tienes? : ";
		cin >> variedadM;
	
		for(y = 1; y <= variedadM; y++){
			int valor2, cantidad2;
			cout << "\nCual es el valor de la moneda? :$";
			cin >> valor2;
			cout << "\nCuantas monedas con ese valor tienes? :";
			cin >> cantidad2;
	
			suma_M = suma_M + ( valor2 * cantidad2 );
		}/*Fin ciclo for 2*/
		total = suma + suma_M;
		/*Impresion de resultados*/
		cout << "\nCuentas con un total de :$" << total<<" pesos.";
}/*Fin metodo main*/	
