/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Miercoles 17 de septiembre del 2020*/

/*Este programa te permite almacenar una cantidad N de calificaciones y 
 * separarlas de acuerdo a su calificacion es decir los que aprueban y los que 
 * no */

#include <iostream>
using namespace std;
/*inicio del metodo principal*/
int main(){
 	/*Declaracion de variables*/
	int aprob = 0, NoAprob, num_alumn, contador = 0, x;
	
	cout << "\nIngresa el numero de alumnos a evaluar :";
	cin >> num_alumn;
	/*Declaracio de arreglos*/
	char nombres[num_alumn][30];
	float calificaciones[num_alumn];
	/*Proceso para almacenar los nombres y calificaciones de los alumnos*/
	for(x = 0; x < num_alumn; x++){
	contador = contador + 1;
	cout << "\nNombre del alumno : ";
	cin >> nombres[x];
	cout << "\ncalificacion : ";
	cin >> calificaciones[x];
		/*Proceso para etiquetar aprobados y reprobados*/
		if((calificaciones[x] >= 6) && (calificaciones[x] <=10)){
			cout << "\nEl alumno "<< nombres[x] << " aprovo con una calificacion  de ["<< calificaciones[x]<<"]";
			aprob = aprob + 1;
		}
		else if ((calificaciones[x] >= 0) && (calificaciones[x] < 6)){
			cout << "\nEl alumno " << nombres[x] << "no aprobo";
			NoAprob = NoAprob + 1;
		}
	}/*impresion de resultados*/
	cout << "\nEL NUMERO DE APROBADOS ES DE [" << aprob << "]";
	cout << "\nEL NUMERO DE REPROBADOS ES DE [" << NoAprob << "]";
}

