/*Raul Hernandez Lopez*/
/*freeenergy1975@gmail.com*/
/*Martes 18 de Agosto del 2020*/

/*Este programa nos permite calcular la cantidad de dinero que tenemos a partir de una cantidad de billetes y monedas definidos por el usuario, el valor de cada moneda y billete tambien es definida por el usuario.*/

#include <iostream>
#include <string>
using namespace std;

int main(){
	/*declaracion de variables*/
	int variedadM, variedadB, total, valor, cantidad, x, y, suma = 0, suma_M = 0;
	/*variable definida por el usuario*/
	cout << "\ningresa la cantidad de billetes con distinto valor con las que dispones :";
	cin >> variedadB;
	/*Recopilacion y procesamiento de datos en nuestro primer ciclo for*/
	for (x = 1; x <= variedadB; x++){
		cout << "\ncual es el valor del billete? :$";
		cin >> valor;
		
		cout << "\ncuantos billetes con ese valor tienes? : ";
		cin >> cantidad;
		
		suma = suma + (valor * cantidad);
	}
	/*Variable definida por el usuario*/
	cout << "\ncuantas monedas con distinto valor tienes? :";
	cin >> variedadM;
	/*Recopilacion y procesamiento de datos desde nuestro segundo ciclo for*/
	for (y = 1; y <= variedadM; y++){
                cout << "\ncual es el valor de la moneda? :$";
                cin >> valor;

                cout << "\ncuantas monedas con ese valor tienes? : ";
                cin >> cantidad;

                suma_M = suma_M + (valor * cantidad);

	}
	/*suma de los valores obtenidos en los 2 ciclos anteriores para posteriormente imprimir resultados*/
	total = suma_M + suma;
	cout<< "\nEl total de la caja es de :$" << total << " pesos";
}
