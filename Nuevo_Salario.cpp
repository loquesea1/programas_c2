/*Raul_Hernandez_Lopez
 * freeenregy1975@gmail.com
 * 16 de octubre del 2020*/

/*Calcular el nuevo salario de un empleado si obtuvo un
 *incremento del 8% sobre su salario actual y un
 descuento de 2.5% por servicios.*/

#include <iostream>
using namespace std;
/*inicio metodo main*/
int main(){
	/*Declaracion de variables*/
	float Salario_Actual, Descuento, Nuevo_Salario, Salario_Final;
	/*Recopila datos*/
	cout << "\nIngresa el valor de tu salario :";
	cin >> Salario_Actual;
	Nuevo_Salario = Salario_Actual + (Salario_Actual * 0.08);
	Descuento = Nuevo_Salario * 0.025;
        Salario_Final = Nuevo_Salario - Descuento;
	/*Imprime resultados*/
	cout << "\nSe te otorgo un incremento del 8%"
        << "\nSin embargo de se te descontara el 2.5 por ciento por servicios\n"
	<< "Sueldo Final [$" << Salario_Final << "]\n";
}/*fin metodo main*/
