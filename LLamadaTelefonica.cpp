/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 20 de agosto del 2020*/

/*Este programa determina el costo de una llamada telefonica con vase a los 
 * minutos que dure*/


#include <iostream>
using namespace std;
/*inicio del metodo main*/
int main(){
	float costo, tiempo, minutos = 1.25;
	/*Recopilacion y procesamiento de datos*/
	cout << "Quieres conocer el costo de tu llamada?";
	cout << "\nEl costo de una llamada por minuto es de : $"<<minutos;

	cout << "\nCuantos minutos duro tu llamada? :";
	cin >> tiempo;

	costo = tiempo * minutos;
	/*impresion de resultados*/
	cout << "\nEl costo de tu llamada es de :$"<< costo<< " pesos\n";	
}
