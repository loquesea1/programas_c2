/*Raul Hernandez Lopez
 * freeenrgy1975@gmail.com
 * 15 de octubre del 2020*/

/*Calcular el descuento y el monto a pagar por un medicamento cualquiera 
 * en una farmacia si todos los medicamentos tienen un descuento del 35%.*/

#include <iostream>
#include <string>
using namespace std;

int main(){
   /*Declaraion de variables*/
   float Precio_Descuento, Precio_Medicamento, Descuento;
   string Medicamento;
   /*Recopila datos*/
   cout << "Nombre del medicamento o producto :";
   cin >> Medicamento;
   cout << "Ingresa el precio real del medicamento o producto :";
   cin >> Precio_Medicamento;
   Precio_Descuento = Precio_Medicamento * 0.65;		
   Descuento = Precio_Medicamento - Precio_Descuento;
   /*Imprime resultados*/
   cout << Medicamento << "\nDescuento [$" << Descuento << "]" 
   << "\nMonto a pagar [$" << Precio_Descuento << "]\n"; 
}
