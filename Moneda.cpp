/*Raul_Hernandez_Lopez*/
/*freeenergy1975@gmail.com*/
/*05 de octubre del 2020*/
/*Realiza la conversion de pesos a dolares o euros*/

#include <iostream>

using namespace std;

int main (){
/*Declaracion de variables*/
float dolares = 22.09, euros = 21.91, conversion, pesos;
int x;
cout << "\nIngresa la cantidad de pesos que tienes : ";
cin >> pesos;
cout << "\nA que tipo de moneda quieres realizar la conversion: 1)Dolares 2)Euros :";
cin >> x;
	/*Realiza la conversion de dolares y Euros*/
	if(x == 1){
		conversion = pesos / dolares;
		cout << "\nLa cantidad de pesos que tienes es de [" << conversion << "] Dolares\n";
	}/*fin if*/
	else if (x == 2){
		conversion = pesos / euros;
		cout << "\nLa cantidad de pesos que tienes es de [" << conversion << "] Euros\n";
	}/*fin else if*/
	else{
		cout << "\nPor favor ingresa una opcion valida\n";
	}/*fin else*/
}/*fin del metodo main*/
