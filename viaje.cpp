/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Lunes 17 de agosto del 2020*/

/*Determina el costo de un boleto de viaje de acuerdo a la distancia de recorrido y el costo por kilometro*/

#include <iostream> 
using namespace std; 

int main(){/*Inicio de nuestro metodo principal*/
	/*Declaracion de funciones*/
	int kilometros = 17, distancia, precio;
	/*Recopilacion y procesamiento de datos*/	
	cout << "\nA que distancia en kilometros se encuentra tu destino? :";
	cin >> distancia;
	
	precio = kilometros * distancia;
	/*Impresion de resulytados*/ 
	cout << "\nEl costo de tu viaje es de :$" << precio << " pesos buen viaje! :D \n";

}/*Fin de nuestro metodo main*/
