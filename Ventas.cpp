/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 03 de septiembre del 2020*/

/*este programa permite almacenar un numero N de ventas y separarla de acuerdo a una serie de
 *condiciones: ventas mayores a 10,000 pero menores de 20,000 Ventas iguales o menores
 de 10,000... cuanto es el monto de cada una y el monto global.*/
#include <iostream>
#include <string>
using namespace std;
/*incio de metodo main*/	
int main(){
	/*declaracion de variables*/
	int x, posicion, numero, mayor = 0, menor = 0; 
	float sumaMayor = 0, sumaMenor = 0, MontoGlobal = 0;
	
	cout << "\n Ingresa tu numero de ventas : ";
	cin >> numero;
	float ventas[numero];

	for(x = 0; x < numero; x++){
		posicion = x + 1;
		cout << "Ingresa el valor la venta No. :"<< posicion << " :";
		cin >> ventas[x];
		/*En esta variable se almacena la suma de todas las ventas*/
		MontoGlobal = MontoGlobal + ventas[x];

		/*antes de que el ciclo continue tiene que pasar por un filtro compuesto de dos 
		 *condicionales*/
		if((ventas[x] >= 0) && (ventas[x] <= 10000)){
			sumaMenor = sumaMenor + ventas[x];
			menor = menor + 1;
		}
		
		else if((ventas[x] >10000) && (ventas[x] <=20000)){
			sumaMayor = sumaMayor + ventas [x];
			mayor = mayor + 1;
		}
		}
	cout << "Realizaste :"<< menor <<" ventas menores a 10000 en total suman : "<< sumaMenor <<"\n";
	cout << "Realizaste :"<< mayor <<" ventas mayores a 10000, en total suman: "<<sumaMayor <<"\n";
	cout << "El monto global es de :"<<MontoGlobal<< "\n";

}/*Fin de del metodo main*/
