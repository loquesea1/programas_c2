/*Raul_Hernandez_Lopez
 * freeenergy1975@gmail.com
 * 15 de octubre del 2020*/

#include <iostream>

using namespace std;
/*inicio metetodo main*/
int main(){
	/*Declaracion de variables*/
	float Tasa_Interes = 0.27;
	float Monto, Prestamo;
	int Fecha_Actual, Fecha_Prestamo, Tiempo_Transcurrido, x ;

	/*Recopila datos*/
	cout << "\nCantidad prestada :";
	cin >> Prestamo;
	cout << "\nIngresa el año de prestamo :";
	cin >> Fecha_Prestamo;
	cout << "\nIngrese la fecha actual :";
	cin >> Fecha_Actual;
	Tiempo_Transcurrido = Fecha_Actual - Fecha_Prestamo;
	/*calcula el monto a pagar en funcion de tasa de interes*/
	for(x = 1; x <= Tiempo_Transcurrido; x++){
		Fecha_Prestamo += 1;
		Prestamo = Prestamo + (Prestamo * Tasa_Interes);
		printf("\nAño [%d] Monto a pagar [$%f]", Fecha_Prestamo, Prestamo);			
	}/*Fin ciclo for*/
}/*Fin del medo main*/
