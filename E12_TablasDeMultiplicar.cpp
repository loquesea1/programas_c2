/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  10   de  enero  del  2021
 * Tema: Imprime la tabla de multiplicar de un numero N*/

#include <iostream>

using namespace std;

int main(){
        int tabla, x, resultado;

        cout << "\nIngresa la tabla de multiplicar \nque quieres que se te muestre: ";
        cin >> tabla;
        //Almacena el valor que corresponde a la tabla de multiplicar.
        cout << "\nTabla de multiplicar del numero\n" << tabla;

        for(x = 1; x <= 10; x++){
                //Impresion de resultados.
                resultado = tabla * x;
                cout << "\n" << tabla << " x " <<  x << " = " << resultado;
        }//Fin for.
        cout << "\n";
}//Fin del metodo main.
