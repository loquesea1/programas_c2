/*Raul_Hernandez_Lopez*/
/*freeenergy1974@gmailcom*/
/*Viernes 02 de octubre del 2020*/
 
/*Calcula el precio de un boleto en funcion de los kilometros a recorrer*/
#include <iostream>
using namespace std;

/*inicio del metodo principal*/
int main(){
	/*Declaracion de variables*/
	float kilometros = 20.5, distancia, costo;
	/*recopilación de datos*/
	cout << "Ingresa la distancia en kilometros de tu destino :";
	cin >> distancia;
	/*calcula el precio del boleto*/
	costo = distancia * kilometros;
	/*impresion de resultados*/
	cout << "El costo de tu boleto es de [$" << costo << "]";
}
