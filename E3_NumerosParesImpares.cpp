/*AUTOR: :  HERNÁNDEZ   LÓPEZ   RAÚL
 *CORREO: freeenergy1975@gmail.com
 *FECHA : 06  de  enero  del  2021
 **/

/* UN  PROGRAMA  SOLICITA  QUE  SEAN  CAPTURADOS  TRES   DATOS
 * NUMERICOS Y A PARTIR DE ELLOS IMPRIMIRA SI EL NUMERO ES PAR.
 **/

#include <iostream>

using namespace std;

int main(){
   //Declaracion de variables.
   int cantidad,
       eleccion,
       x;
      //Imprime un mensaje que pone en contexto al usuario
      cout << "\nA  continuacion  se  te solicitara que ingreses " << 
              "\npara números para determinar si son par o impar";

   do{
       //Determina el numero de veces que se ejecutara el ciclo for
       cout <<  "\n\nCuantos números ingresaras? : ";
       cin >> cantidad;

       int numeros[cantidad];


       for(x = 0; x < cantidad; x++){

           //Captura de datos
           cout << "\nNúmero : ";
           cin >> numeros[x];

           //Evalua si el número es par o no.
           if(numeros[x]%2 == 0){
              cout << "El número es par\n" <<
                     "_________________\n";
           }//Fin if
           else{
             cout << "El número es impar\n",
                     "_________________\n";

            }//Fin else
       }//Fin for
   cout << "\n\nDeseas continuar\n1)yes\n0)no:" ;
   cin >> eleccion;

   }while(eleccion == 1);
}//Fin metodo principal

