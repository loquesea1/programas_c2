/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 13 de agosto del 2020*/

/* Este programa determina el total de dinero dentro de un monedero en el cual 
 * se conoce con certeza el valor de cada elemento*/

#include <iostream>
using namespace std;
/*Inicio de nuestro metodo main*/
int main(){
	/*Declaracion de variables, y creacion de nuestro arreglo monedero que 
 	* contiene los elementos dentro del monedero*/
	int x, y, suma = 0, valor = 0, suma2 = 0, valor2 = 0, total = 0;
	int monedero[] = {1 ,5 , 10, 20, 50};
	
	cout << "Monedero\n\n";
	/*recopilacion de datos haciendo uso de un ciclo for que hace refencia a 
 	* nuestro arreglo "monedero*/
	for(x = 0; x <= 2; x++){
		cout << "\ncuantas monedas de :$"<< monedero[x]<<" tiene? :";
		cin >> valor;
		
		suma = suma + (monedero[x] * valor);
 
	}
	/*Segunda recopilacion de datos haciendo uso de otro ciclo for que hace referncia
 	*a nuestro arreglo "monedero"*/ 
	for(y = 3; y <=4; y++){
		cout << "\ncuantos billetes de $"<< monedero[y]<< " tiene? :";
		cin >> valor2;

		suma2 = suma2 + ( valor2 * monedero[y]);
	}
	/*Suma de los valores obtenidos en los ciclos anteriores*/
	total = suma2 +	 suma;
	/*impresion de resultados*/
	cout << "\nCuentas con un total de :$"<<total<<" pesos"; 
}
/*Fin de nuestro metodo main*/
