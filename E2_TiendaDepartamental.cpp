/*AUTOR:  HERNÁNDEZ   LÓPEZ   RAÚL
 *CORREO: freeenergy1975@gmail.com
 *FECHA : 06  de  enero  del  2021
 * */

/* TEMA :EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
 * TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE
 * FRECUENTE. LES OTORGARAN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
 */

#include<iostream>

using namespace std;

int main(){
    //Declaración de variables.
    typedef char nombre[20];
    int eleccion,
         tarjeta,
         estadoCompras,
         x = 0;
    nombre producto[30];
    float precio[30],
          sumaPrecio = 0,
          descuento = 0.80,
          montoConDescuento;
    do{
        sumaPrecio = 0;
        do{
          //Entrada de datos.
          cout << "\nNombre del producto: ";
          cin >> producto[x];
          cout << "\nPrecio del producto $";
          cin >> precio[x];
          //Almacena el valor de la suma de todos los productos.
          sumaPrecio += precio[x];
          x++;
          //Verifica que todos los productos se registren         
          cout << "\nTerminaste de resgistrar tus compras?\n1)yes\n0)no : ";
          cin >> estadoCompras;

        }while(estadoCompras == 0 );//Fin do while 2

        cout << "________________________________\n" <<
               "\nCuentas con tarjeta de cliente frecuente?\n1)yes\n0)no : ";
        cin >> tarjeta;

        //Evalua si el cliente cuenta con tarjeta o no.
        if(tarjeta == 1){
            montoConDescuento = sumaPrecio * descuento;
            cout  << "\nEl Monto a pagar es de $" <<  montoConDescuento <<
                  "\n________________________________\n";
        }//Fin if
        else{
            cout << "\n_______________________________" <<
                   "\nMonto a pagar es $" << sumaPrecio <<
                   "\n_______________________________\n";
        }//Fin else 

        //Le da la opcion al cliente de volber a ejecutar el programa.
        cout << "\nDesea continuar\n1)yes\n0)no : ";
        cin >> eleccion;
    }while(eleccion == 1);//Fin do while 1          
}//Fin metodo principal

