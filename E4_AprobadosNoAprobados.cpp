/* Autor: HERNANDEZ LOPEZ RAUL @Neo
 * correo: freeenergy1975@gmail.com
 * fecha: 08  de   enero  del  2021
 * Tema: Genera un programa que determine si la calificacion es aprobatoria o no*/

#include <iostream>

using namespace std;

int main(){
    //Declaracion de variables.
    int x = 0, eleccion;
    float calificacion [50];
    typedef char nombre[20];
    nombre nombreAlumnos[50];

    cout << "\nA  continuacion  se  te" <<
           "solicitara que ingreses\n" <<
           "una  calificacion en un\n"<<
           "rango del 1 al 100\n"<<
           "_______________________\n";

    do{
        //Entrada de datos.
        cout << "\nNombre: ";
        cin >> nombreAlumnos[x];

        cout << "\nCalificacion: ";
        cin >> calificacion[x];

        if((calificacion[x] >= 1) && (calificacion[x] <= 100)){
            if(calificacion[x] >= 60){
                cout << "\nFelicidades has aprobado!" << 
                       "\n_________________________";
            }//Fin if 
            else{
                cout << "\nNo has aprobado!\n" <<
                       "_______________";
            }//Fin else anidado
        }//Fin if

        else{
            cout << "\nEl valor ingresado no es valido\n" <<
                   "por favor vuelve a comenzar\n" <<
                   "_______________________________\n";
            eleccion = 1;
        }//Fin else

        //Despliega un pequeño menú para volver a ejecutar el programa.
        cout << "\nDeseas continuar 1)yes 0)no :";
        cin >> eleccion;

        x++;
    }while(eleccion == 1);
}//Fin metodo principal

