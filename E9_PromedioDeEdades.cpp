/* AUTOR:  Hernandez   Lopez   Raul
* correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema:CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE*/

#include <iostream>

using namespace std;

int main(){
        //Declaracion de variables
        int numAlumnos, x;
        float promedioEdades, sumaEdades = 0;

        cout <<"\nA continuacion calcularemos el \npromedio de  edades  de  primer \nsemestre.\n";

        //Dtermina el tamaño del arreglo
        cout << "\nNumero de edades a capturar: ";
        cin >> numAlumnos;
        int edades[numAlumnos];

        for(x = 0; x < numAlumnos; x++){
                //Captura de datos.
                cout <<  "\n[" << x+1 <<  "] Edad: ";
                cin >> edades[x];

                sumaEdades += edades[x];
        }//Fin for.
        //Impresion de resultados
        promedioEdades = sumaEdades / numAlumnos;

        cout << "\nEl promedio de edades del grupo es: " << promedioEdades << "\n";
}//Fin metodo main.

