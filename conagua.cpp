/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Vernes 09 de Octubre del 2020*/

/*determinar el pago que debe realizar una persona por el 
total de metros cúbicos que consume de agua*/

#include <iostream>
using namespace std;
/*inicio metodo main*/
int main(){
	/*Declaracion de variables*/
	float pago, costo_agua, metros_consumido;
	/*Recopilacion de datos*/
	cout << "\nIngresa el costo por metro cubico de agua : ";
	cin >> costo_agua;
	cout << "\nIngresa los metros cubicos de agua que consumiste : ";
	cin >> metros_consumido;
	/*Define el monto a pagar*/
	pago = metros_consumido * costo_agua;
	/*impresion de resultados*/
cout << "\nTu pago es de [$" << pago << "]";
}/*fin metodo main*/
