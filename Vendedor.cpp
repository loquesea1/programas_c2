/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Lunes 26 de Octubre del 2020*/

/*Un vendedor recibe un sueldo base más un 10% extra por comisión de sus ventas
el vendedor desea saber cuánto dinero obtendrá por concepto de comisiones por 
las tres ventas que realiza en el mes y el total que recibirá en el mes tomando
en cuenta su sueldo base y comisiones.*/

#include <iostream>

using namespace std;

int main(){
    /*Declaracion de variables*/
    float comision = 0, sueldoFijo, sueldoTotal, venta;
    int numeroVentas, posicion = 0, x;
    /*Recopilacion de datos*/
    cout << "\nIngresa el monto de tu sueldo actual $";
    cin >> sueldoFijo;
    cout << "\nIngresa el numero de ventas que realizaste :";
    cin >> numeroVentas ;
    /*Determina el monto correspondiente por concepto de ventas*/
    for(x = 0; x < numeroVentas; x++){
        posicion = posicion + 1;
        cout << "\nIngresa el numero la venta [" << posicion << "] :";
        cin >> venta;
        comision =+ (venta * 0.10);
    }/*fin ciclo for*/
    /*Calcula el sueldo total correspondiente*/
    sueldoTotal = comision + sueldoFijo;
    /*Impresion de resultados*/
    cout << "\nel monto recabado por comision de venta es de [$" << comision << "]";
    cout << "\nTu sueldo total es de $" << sueldoTotal;
}/*Fin metodo main*/
