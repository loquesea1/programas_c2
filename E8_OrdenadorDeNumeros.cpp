/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema: Elabora un programa que compare tres numeros y los ordene de mayor a menor*/

#include <iostream>

using namespace std;

int main(){
        //Declaracion de variables
        int cambioPosicion, numElements, x, y, z, v;

        cout << "\nCuantos numeros ingresaras?: ";
        cin >> numElements;
        int ordenar[numElements];

        //Entrada de datos
        for(x = 0; x < numElements; x++){
            cout << "\nNumero: ";
            cin >> ordenar[x];
        }// Fin for

        for(y = 0; y < numElements; y++){
             for(z = 0; z < numElements-1; z++){
                if(ordenar[z] < ordenar[z+1]){
                        cambioPosicion = ordenar[z];
                        ordenar[z] = ordenar[z+1];
                        ordenar[z+1] = cambioPosicion;
                }//Fin if
             }//Fin for 2       
        }//Fin for 3

        cout << "\nEl orden es el siguiente :";
        for(v = 0; v < numElements; v++){
                cout <<ordenar[v] << " " ;
        }//fin for 4
        cout << "\n";
}//Fin metodo principal
