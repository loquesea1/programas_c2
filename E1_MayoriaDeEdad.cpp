/* AUTOR: HERNÁNDEZ   LÓPEZ    RAÚL
 * CORREO: freeenergy1975@gmail.com
 * FECHA : 06  de  Enero  del  2021
 */

//TEMA  :Genera un programa que determine si eres de mayor de edad

#include <iostream>

using namespace std;

int main(){
   //Declaracion de variables.
   int     fechaNacimiento,
           fechaActual,
           edad,
           eleccion;

   do{
        //Recoleccion de datos  
        cout << "\nFecha de nacimiento :";
        cin >> fechaNacimiento;
        cout << "\nFecha actual :";
        cin >> fechaActual;

        //Determina el valor de la edad
        edad = fechaActual - fechaNacimiento;

        //Evalua si el dato introducido se encuentra dentro del rango establecido.
        if ((edad >= 1) && (edad <= 120)){
            //Evalua si el dato es mayor o igual a 18 
            if (edad >= 18){
                cout << "\nEres mayor de edad\n_______________________\n";

            }//Fin if anidado
            else {
                cout << "\nEres menor de edad\n_______________________\n";
            }//Fin else anidado.
        }//Fin if. 

        else{
            cout << "el valor ingresado esta fuera de rango\n%s%s" <<
               "por favor ingresa un dato valido." <<
               "\nreiniciando ...\n_____________________________________\n";
        }//Fin else

         cout << "\nDeseas continuar ?\n1)yes\n0)no :";
         cin >> eleccion;

    }while(eleccion == 1);
}//Fin metodo principal

