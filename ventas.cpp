/*
 Fecha: 04-04-2020
 Autor: Fatima Azucena MC
 <fatimaazucenamartinez274@gmail.com
*/
#include <iostream>
using namespace std;
    //Un vendedor ha realizado N ventas y 
    //desea saber cu�ntas fueron por 10,000 o menos, 
    //cu�ntas fueron por m�s de 10,000 pero por menos de 20,000, 
    //y cu�nto fue el monto de las ventas de cada una y el monto global. 
    //Realice un algoritmo para determinar los totales. Represente la soluci�n

int main(){
    //Definicion de variables
    int numVenta;
    float montoGlobal = 0;
    float montoMADE = 0;
    float montoMEDE = 0;
    int p;
    int v;
    //ASignacion de variables
    //Ingresa datos por teclado
    cout<<"Ingrese numero de ventas:"<<'\n';
    cin>>numVenta;
    int g[numVenta];
    //Inicio for
    for(p=1;p<=numVenta;p++){
        cout<<"Ingrese el precio de cada venta:"<<'\n';
        cin>> g[p];
    }//FIn for
    //Inicio segundo for
    for(v=1;v<=numVenta;v++){
        if (g[v] <=10000){
            montoMEDE=montoMEDE + g[v];
            montoGlobal=montoGlobal + g[v];
        }//Fin segundo for
        else if (g[v] >=10000 && g[v]< 20000){
            montoMEDE=montoMEDE + g[v];
            montoGlobal=montoGlobal + g[v];
        }
    }
    cout<<"El monto global es: "<<montoMADE<<" + "<<montoMEDE<<" = "<<montoGlobal;
}//Fin main
