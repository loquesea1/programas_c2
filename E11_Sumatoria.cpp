/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  10   de  enero  del  2021
 * Tema: Calcula la sumatoria de un numeros*/

#include <iostream>

using namespace std;

int main(){
    //Declaracion de variables
    int x, sumatoria = 1, numero;

    cout << "\nSumatoria de números\n";

    //Almacena el numero para calcular la sumatoria
    cout << "\nIngresa un número entero positivo: ";
    cin >> numero;

    for( x = 1; x <= numero; x++){
        sumatoria += x;
        cout << "\nLa sumatoria de " << x << " = " << sumatoria << "\n";
    }//Fin for
}//Fin del metodo main 

