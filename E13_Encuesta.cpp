/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  25   de  enero  del  2021
 * EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER 
       MAYOr
       INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O 
       HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN 
       HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
        • EDAD
        • LOCALIDAD
        • TIENEN INTERNET EN CASA
        • TIENEN COMPUTADORA EN CASA
        • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
    2. RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE 
       DESCONOCE LA CANTIDAD DE ALUMNOS. 
    3. IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES.*/

#include <iostream>

using namespace std;

int main(){
        int edad,   localidad,   internet,   computadora,  alimentacion,  numAlumnos,
            sinInternet = 0, conInternet = 0, conComputadora = 0, sinComputadora = 0,
            buenaAlimentacion = 0, malaAlimentacion = 0, numJilotepec = 0, numVicente = 0,
            numDanxho = 0, numFresno = 0, respuesta, x = 0;
        float porcentajeInternet ,  porcentajeSinInternet ,  porcentajeComputadora ,
              porcentajeSinComputadora, porcentajeBuenaAlimentacion,
              porcentajeMalaAlimentacion, porcentajeJilotepec, porcentajeVicente,
              porcentajeDanxho, porcentajeFresno, porcentaje;

    do{
           x++;
           cout << "\nEdad: ";
           cin >> edad;

           cout << "\nLocalidad\n1)Jilotepec de Molina\n2)San Vicente\n3)Danxho\n4)El fresno" <<
                           "\nDigita tu opcion: ";
           cin >> localidad;

           switch (localidad){
               case 1:
                 numJilotepec += 1;
               break;

               case 2:
                  numVicente += 1;
               break;

               case 3:
                  numDanxho += 1;
               break;

               case 4:
                  numFresno += 1;
               break;
           }//Fin switch case.\

           cout << "\nCuenta con internet en casa ?\n1)yes 0)No: ";
           cin >> internet;

           if(internet == 1){
                conInternet += 1;
           }//Fin if 1.
           else{
                sinInternet += 1;
           }//Fin else 1.

           cout << "\nTiene conputadora\n1)yes 0)No: ";
           cin >> computadora;

           if(computadora == 1){
                conComputadora += 1;
           }//Fin if 2.
           else{
                sinComputadora += 1;
           }//Fin else 2.

           cout << "\nTiene una buena alimentacion\n1)yes 2)no: ";
           cin >> alimentacion;

           if(alimentacion == 1){
                buenaAlimentacion += 1;
           }//Fin if 3.
           else{
                malaAlimentacion += 1;
           }//Fin else 3.
        cout << "\nSiguiente\n1)yes 0)No: ";
        cin >> respuesta;

        }while(respuesta == 1);
        //Procesamiento de datos
        porcentaje = 100/x;
        //Calcula el procentaje corresponiente a cada localidad
        porcentajeJilotepec = porcentaje * numJilotepec;
        porcentajeVicente = porcentaje * numVicente;
        porcentajeDanxho = porcentaje * numDanxho;
        porcentajeFresno = porcentaje * numFresno;
        //Calcula el porcentaje de aquellos que tiene internet y los que no.
        porcentajeInternet = porcentaje * conInternet;
        porcentajeSinInternet = 100 - porcentajeInternet;
        //Calcula el porcentaje de los alumnos que tienen computadora y los que no.
        porcentajeComputadora = porcentaje * conComputadora;
        porcentajeSinComputadora = porcentaje * sinComputadora;
        /*Calcula el porcentaje correspondiente del numero de alumnos que tiene buena 
        alimentacion y los que no.*/
        porcentajeBuenaAlimentacion = porcentaje * buenaAlimentacion;
        porcentajeMalaAlimentacion = porcentaje * malaAlimentacion;\
        //Impresion de resultados.
        cout << "\n" << "Resultados de la encuesta      |      Numero de alumnos: " <<  x <<
               "\n___________________________________________________________\n" <<
               "\nDescripcion\t\t\tAlumnos\t\t%"<<
               "\n___________________________________________________________\n"<<
               "\nCon conexion a internet\t\t" << conInternet << "\t\t" << porcentajeInternet <<
               "\nSin conexion a internet\t\t" << sinInternet << "\t\t" << porcentajeSinInternet <<
               "\nCuentan con computadora\t\t"<< conComputadora << "\t\t" << porcentajeComputadora <<
               "\nNo tienen computadora\t\t" << sinComputadora << "\t\t" << porcentajeSinComputadora <<
               "\nBuena alimentacion\t\t" << buenaAlimentacion << "\t\t" << porcentajeBuenaAlimentacion <<
               "\nMala Alimentacion\t\t" << malaAlimentacion << "\t\t" << porcentajeMalaAlimentacion <<
               "\nProcedentes de Jilotepec\t" << numJilotepec << "\t\t" << porcentajeJilotepec <<
               "\nProcendetes de San vicente\t" << numVicente << "\t\t" << porcentajeVicente <<
               "\nProcedentes de Danxho\t\t" << numDanxho << "\t\t" << porcentajeDanxho <<
               "\nProcedentes deL Fresno\t\t" << numFresno << "\t\t" << porcentajeFresno << "\n";
}//Fin metodo principal.

