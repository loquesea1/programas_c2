/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Viernes 28 de agosto del 2020*/

/*Este programa determina el tiempo que tarda una persona en hacer un recorrido*/

#include <iostream>
using namespace std;
/*Inicio de nuestro metodo main*/
int main(){
	/*Declaracion de variables*/
	float velocidad, distancia, tiempo;
	/*Recopilacion y procesamiento de datos*/
	cout << "\nCual es la distancia en km que recorriste? : ";
	cin >> distancia;
	
	cout << "\nA que velocidadm 'km/hr' ? ";
	cin >> velocidad;
	
	tiempo = distancia / velocidad;
	/*Impresion de resultados*/
	cout << "\nEl tiempo que tardaste en recorrer "<< distancia<< " km es de :"<< tiempo<< "hrs\n";
}/*Fin de nuestro metodo main*/
