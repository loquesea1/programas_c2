/*Raul_Hernandez_Lopez
 * freenergy1975@gmail.com
 * Miercoles 09 de septiembre del 2020*/

#include <iostream>
using namespace std;
/*Inicio del metodo main*/
int main(){
	/*Declaracion de variables*/
	int x, y, z, w, tam, New_Ord;
	cout << "\nIngresa el numero de elementos a ordenar :";
	cin >> tam;
	int orden[tam];
	/*catura de los valores dentro del arreglo*/
	/*For 1*/
	for(x = 0; x < tam; x++){
		cout << "\nIngresa el valor :";
		cin >> orden[x];
	}/*Fin for 1*/
	/*Inicio for 2*/
	for(y = 0; y < tam; y ++){
		/*Ordenamiento de datos*/
		/*Inicio for 3*/
		for(z = 0; z < tam-1; z++){
			/*Inicio del if*/
			if(orden[z] > orden[z+1]){
				New_Ord = orden[z+1];
				orden[z+1] = orden[z];
				orden[z] = New_Ord;
			}/*Fin del if*/
		}/*Fin for 3*/
	}/*Fin for 2*/
	/*impresion de resultados*/
	cout << "\nOrden de manera ascendente :";	
	for(w = 0; w < tam; w++){
		cout << orden[w];
	}
}/*Fin del metodo principal*/
