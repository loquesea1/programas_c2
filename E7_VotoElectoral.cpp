/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Programa que determina si una persona es apta  para  votar  si es
 * mayor  de  edad,  cuenta  con  credencial  para votar, es vigente
 * y si es que se encuentra en el patron electoral de no cumplir con 
 * las especificaciones se considera como no apto.
 */

#include <iostream>

using namespace std;

int main(){
   //Declaracion de variables.  
   int edad, vigencia, credencial,
       patronElectoral;

   cout << "\nIngresa tu edad: ";
   cin >> edad;

   if(edad >= 18){
        cout << "\nCuenta con credencial para votar?: " <<
               "\n1)Si\n0)No: ";
        cin >> credencial;

        if(credencial == 1){
                cout << "\nFecha de vigencia por ejemplo '2001': ";
                cin >> vigencia;

                if(vigencia >= 2006){
                        cout << "\nSe encuentra en el patron electoral?" <<
                               "\n1)Yes\n0)No: ";
                        cin >> patronElectoral;

                        if(patronElectoral == 1){
                                cout << "\nEres apto para votar" <<
                                       "\n____________________\n";
                        }//Fin if 4
                        else{
                             cout << "\nNo eres apto para votar" <<
                             "\n_______________________\n";
                        }//Fin else 4
                }//Fin if 3
                else{
                     cout << "\nNo eres apto para votar" <<
                     "\n_______________________\n";
                }//Fin else 3
        }//Fin if 2.
        else{
             cout << "\nNo eres apto para votar" <<
               "\n_______________________\n";
        }//Fin else 2.
   }//Fin if 1
   else{
        cout << "\nNo eres apto para votar" <<
               "\n_______________________\n";
   }//Fin else 1
}//Fin metodo principal.

