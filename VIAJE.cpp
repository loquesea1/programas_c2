/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*viernes 02 de Octubre del 2020*/

/*Calcula el precio de un de un boleto en funcion de los kilometros recorridos*/

#include <iostream>

using namespace std;

int main(){
	/*Declaracion de variables*/
	float kilometros = 20.5, distancia, precio;
	/*Recopilacion de datos*/
	cout << "\nIngresa la distancia en kilometros de tu destino :";
	cin >> distancia;
	/*calula el valor del boleto*/
	precio = distancia * kilometros;
	/*impresion de resultados*/
	cout << "\nEl costo de tu boleto es de [$" << precio << "]";

}/*fin del metodo main*/

